package ls.dbcloner

import org.springframework.jdbc.core.{ArgumentPreparedStatementSetter, BatchPreparedStatementSetter, RowCallbackHandler, JdbcOperations}
import java.sql.{PreparedStatement, ResultSet}

class Cloner(val source: JdbcOperations, val target: JdbcOperations) {

    val BATCH_SIZE = 5000

    def deleteAll(table:String){
      println("Deleting all rows from table %s".format(table))
      target.update("delete from %s".format(table))
    }


    def copyTableData(table: String){
      var count = 0L;
      val argSetter = new ArgumentPreparedStatementSetter(Array.empty){
        override def setValues(ps: PreparedStatement): Unit = {
             ps.setFetchSize(BATCH_SIZE)
        }
      }
      var batch: List[Array[AnyRef]] = Nil
      var insertStatement:String = null
      source.query(String.format("select * from %s",table),argSetter, new RowCallbackHandler {


        override def processRow(rs: ResultSet): Unit = {
          if(insertStatement == null){
            insertStatement = _createInsertStatement(rs, table)
          }
          count = count + 1
          println("Reading table %s row %s...".format(table, count))
          batch = batch :+ _extractArgs(rs)
          if(count%BATCH_SIZE == 0){
            println("Inserting batch of %s elements".format(batch.size))
            update(insertStatement, batch)
            batch = Nil
          }
        }
      })


      if(!batch.isEmpty){
        println("Inserting batch of %s elements".format(batch.size))
        update(insertStatement, batch)
      }
      println("Copy of table %s completed...".format(table, count))
    }

  def updateSequence(sequence: String){
    //implementation for oracle only, TODO other
    var originalIncrement = 0;
    val value = source.queryForInt("select %s.nextval from dual".format(sequence))
    var current = target.queryForInt("select %s.nextval from dual".format(sequence))
    while(current < value){
      println("Updating sequence %s to next value. Current value: %s, Target: %s".format(sequence, current, value))
      if(originalIncrement == 0){
        var newCurrent = target.queryForInt("select %s.nextval from dual".format(sequence))
        originalIncrement = newCurrent - current;
        current = newCurrent
      }else{
        target.update("alter sequence %s increment by %s".format(sequence, value-current))
        current = target.queryForInt("select %s.nextval from dual".format(sequence))
      }
    }
    if(originalIncrement != 0){
      target.update("alter sequence %s increment by %s".format(sequence, originalIncrement))
    }
    println("New sequence %s value is %s. Target %s".format(sequence, current, value))
  }

  protected def update(insertStatement: String, args: List[Array[AnyRef]]){
    val setter = new BatchPreparedStatementSetter {

      override def getBatchSize: Int = args.size

      override def setValues(ps: PreparedStatement, i: Int): Unit = {
        args(i).zipWithIndex.foreach( tuple => {
          ps.setObject(tuple._2+1, tuple._1)
        })
      }
    }
    target.batchUpdate(insertStatement, setter)
  }

    protected def _createInsertStatement(rs: ResultSet, table: String): String = {
      val metadata = rs.getMetaData
      var st = "insert into %s ".format(table)
      val columns = (1 to metadata.getColumnCount).map(i => {
        metadata.getColumnName(i)
      })
      val placeHolders = (1 to metadata.getColumnCount).map(i => {
        "?"
      })
      st = st + columns.mkString("(", ",", ") ")
      st + placeHolders.mkString("VALUES(", ",", ")")
    }

    protected def _extractArgs(rs: ResultSet): Array[AnyRef] = {
      val metadata = rs.getMetaData
      (1 to metadata.getColumnCount).map(i => {
        rs.getObject(i)
      }).toArray
    }
 }