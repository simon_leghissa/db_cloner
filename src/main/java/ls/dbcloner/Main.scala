package ls.dbcloner

import org.springframework.jdbc.core.{JdbcTemplate, JdbcOperations}
import org.apache.commons.dbcp.BasicDataSource
import org.apache.commons.cli._
import scala.io.Source

object Main {


  def main(args: Array[String]): Unit = {
    println("Starting Db Cloner...")

    try{
      val config = parseOptions(args)
      val cloner = new Cloner(
        createJdbcOperations(config.sourceDb),
        createJdbcOperations(config.targetDb)
      )

      var results:List[String] = Nil
      
      if(config.tablesFile != null){
        if(config.forceDelete) {
          results = results ::: deleteAllRows(config.tablesFile, cloner)
        }
        results = results ::: copyTablesData(config.tablesFile, cloner)
      }
      
      if(config.sequencesFile != null){
        results = results ::: updateSequences(config.sequencesFile, cloner)
      }

      println(results.mkString("\n"))
    }catch{
      case e:MissingOptionException => System.exit(-1)
    }
  }

  private def deleteAllRows(tablesFile: String, cloner: Cloner): List[String] = {
    Source.fromFile(tablesFile).getLines().toList.map(table => {
      try{
        cloner.deleteAll(table)
        "%s: %s".format(table, "Success deleting")
      }catch{
        case e:Exception => {
          "%s: %s".format(table, e.getMessage)
        }
      }
    })
  }

  private def copyTablesData(tablesFile: String, cloner: Cloner): List[String] = {
      println("Starting tables data copy from file %s".format(tablesFile))
      Source.fromFile(tablesFile).getLines().toList.reverse.map(table => {
      try{
        cloner.copyTableData(table)
        "%s: %s".format(table, "Success copying data")
      }catch{
        case e:Exception => {
          "%s: %s".format(table, e.getMessage)
        }
      }
    })
  }

  private def updateSequences(sequencesFile: String, cloner: Cloner): List[String] = {
    println("Starting sequences update from file %s".format(sequencesFile))
    Source.fromFile(sequencesFile).getLines().map(sequence => {
      try{
        cloner.updateSequence(sequence)
        "%s: %s".format(sequence, "Success updating sequence")
      }catch{
        case e:Exception => {
          "%s: %s".format(sequence, e.getMessage)
        }
      }
    }).toList
  }

  private def createJdbcOperations(info: DbInfo) :JdbcOperations = {
    val ds = new BasicDataSource()
    ds.setDriverClassName(info.driverClassName)
    ds.setUrl(info.url)
    ds.setUsername(info.username)
    ds.setPassword(info.password)
    new JdbcTemplate(ds)
  }

  private def parseOptions(args: Array[String]):Config = {
    val options = new Options
    options.addOption("sDrv", "sourceDriver", true, "jdbc driver class for source database")
    options.addOption("sUrl", "sourceUrl", true, "jdbc url for source database")
    options.addOption("sUsr", "sourceUsername", true, "username for source database")
    options.addOption("sPsw", "sourcePassword", true, "password for source database")
    options.addOption("tDrv", "targetUrl", true, "jdbc driver class for target database")
    options.addOption("tUrl", "targetUrl", true, "jdbc url for target database")
    options.addOption("tUsr", "targetUsername", true, "username for target database")
    options.addOption("tPsw", "targetPassword", true, "password for target database")
    options.addOption("tl", "tableList", true, "tables to be copied")
    options.addOption("sl", "sequenceList", true, "sequences to be updated")
    options.addOption("d", "delete", false, "delete all rows in tables")

    val commandLine = new PosixParser().parse(options, args)
    new Config(
      new DbInfo(getValue("sDrv", commandLine, options),getValue("sUrl", commandLine, options),getValue("sUsr", commandLine, options),getValue("sPsw", commandLine, options)),
      new DbInfo(getValue("tDrv", commandLine, options),getValue("tUrl", commandLine, options),getValue("tUsr", commandLine, options),getValue("tPsw", commandLine, options)),
      getMaybeValue("tl", commandLine),
      getMaybeValue("sl", commandLine),
      commandLine.hasOption("d")
    )
  }


  class Config (val sourceDb: DbInfo, val targetDb: DbInfo, val tablesFile: String, val sequencesFile: String, val forceDelete: Boolean){
  }


  class MissingOptionException(message:String) extends RuntimeException{}

  private def getValue(option: String, commandLine: CommandLine, options:Options): String ={
    if (commandLine.hasOption(option)) {
      return commandLine.getOptionValue(option)
    }
    val help = new HelpFormatter
    help.printHelp("dbCloner", options)
    throw new MissingOptionException("option value not found %s".format(option))
  }

  private def getMaybeValue(option: String, commandLine: CommandLine): String ={
    if(commandLine.hasOption(option)){
      commandLine.getOptionValue(option)
    }else{
      null
    }
  }

}