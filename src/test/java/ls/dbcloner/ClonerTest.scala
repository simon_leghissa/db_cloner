package ls.dbcloner

import org.scalatest.FunSuite
import org.apache.commons.dbcp.BasicDataSource
import org.springframework.jdbc.core.{ArgumentPreparedStatementSetter, PreparedStatementSetter, JdbcOperations, JdbcTemplate}

class ClonerTest extends FunSuite {

//  test("bla"){
//    val target = createJdbcOperations(new DbInfo("oracle.jdbc.OracleDriver", "jdbc:oracle:thin:@//localhost:1521/orcl", "ipadd_owner", "ipa"))
//    target.update("insert into IPADD_ROLE VALUES(100, 'asd')")
//    println("juppy")
//  }


  test("can clone data"){
    val source = createJdbcOperations(new DbInfo("org.h2.Driver", "jdbc:h2:mem:source;MODE=Oracle", "sa", ""))
    val target = createJdbcOperations(new DbInfo("oracle.jdbc.OracleDriver", "jdbc:oracle:thin:@//localhost:1521/orcl", "ipadd_owner", "ipa"))

    createTable(source)
    createTable(target)
    val insertsCount = 200000
    (0 until insertsCount).foreach(i => {
      insert(source, Array(i.asInstanceOf[AnyRef], "text"+i))
    })
    println("inserted, now cloning")
    new Cloner(source, target).copyTableData("test")

    val result = target.queryForList("select * from test")

    println(result)
    assert(result.size() == insertsCount)
  }

  def createTable(jdbcOperations: JdbcOperations){
    jdbcOperations.execute("create table test (id numeric primary key,text varchar2(255))")
  }

  def insert(jdbcOperations: JdbcOperations, args:Array[AnyRef]){
    jdbcOperations.update("insert into test values(?,?)", new ArgumentPreparedStatementSetter(args))
  }

    private def createJdbcOperations(info: DbInfo) :JdbcOperations = {
      val ds = new BasicDataSource()
      ds.setDriverClassName(info.driverClassName)
      ds.setUrl(info.url)
      ds.setUsername(info.username)
      ds.setPassword(info.password)
      new JdbcTemplate(ds)
    }

}
