# README #

Small CLI app created ad-hoc to clone data from an Oracle DB schema to another.
At the current stage it performs only data and indexes update, does not create the schema.

Steps to run the app:
 - mvn clean package
 - java -jar target/DbCloner-1.0-SNAPSHOT.jar

Usage: dbCloner
 -d,--delete                    delete all rows in tables
 -sDrv,--sourceDriver <arg>     jdbc driver class for source database
 -sl,--sequenceList <arg>       sequences to be updated
 -sPsw,--sourcePassword <arg>   password for source database
 -sUrl,--sourceUrl <arg>        jdbc url for source database
 -sUsr,--sourceUsername <arg>   username for source database
 -tDrv,--targetUrl <arg>        jdbc driver class for target database
 -tl,--tableList <arg>          tables to be copied
 -tPsw,--targetPassword <arg>   password for target database
 -tUrl,--targetUrl <arg>        jdbc url for target database
 -tUsr,--targetUsername <arg>   username for target database

